# Projet Restauration

## Structure du projet

Le projet est cindé en plusieurs module :
- Le module Applicatif
- Le module Serveur
- Le module Services

### Module Application

Ce module contient toutes les vues :
- Formulaires (ajout, modification, suppression, connexion)
- Listes (produits, historique des commandes, utilisateurs, restaurants)
- Menus (gestionnaire, administrateur)

Les liens entre chaque vue sont effectués dans la classe HomeFrame.

Les objets User, Restaurant et Product sont initialisés dans leur classe respective dans le package "domain".

Les classes Products, Users et Restaurants présentes dans le package "services" se composent d'un tableau permettant l'ajout de plusieurs User, Product et Restaurant pour les afficher sous forme de liste. 

### Module Serveur

Il contient les migrations FlyWay ainsi qu'un resource bundle nommé config.properties.
La modification de ce document est impérative pour pouvoir appliquer les migrations.

Il contient également le singleton permettant de se connecter à la base de données étant prêt à fonctionner avec un formulaire java.

### Module Services

Nous n'avons pas eu le temps de travailler sur ce module étant donné la deadline très proche.


