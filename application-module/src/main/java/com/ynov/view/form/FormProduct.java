package com.ynov.view.form;

import com.ynov.domain.Product;
import com.ynov.service.Products;
import com.ynov.view.HomeFrame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FormProduct extends JPanel implements ActionListener {

    private final HomeFrame app;
    private Products products;

    JLabel productName = new JLabel("Nom du produit");
    JLabel quantity = new JLabel("Quantité (unitaire)");
    JLabel price = new JLabel("Prix unitaire");

    JTextField inpProductName = new JTextField(10);
    JTextField inpQunatity = new JTextField(5);
    JTextField inpPrice = new JTextField(5);

    JButton validate = new JButton("Ajouter");
    JButton backToMenu = new JButton("Retour au menu");

    public void formProduct(){
        this.add(productName);
        this.add(inpProductName);
        this.add(quantity);
        this.add(inpQunatity);
        this.add(price);
        this.add(inpPrice);
        this.add(validate);
        this.add(backToMenu);

        validate.addActionListener(this);
        backToMenu.addActionListener(e -> app.menuGestionnaire());
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent){
        String getInpProductName = inpProductName.getText();
        int getInpQuantity = Integer.parseInt(inpQunatity.getText());
        int getInpPrice = Integer.parseInt(inpPrice.getText());

        try{
            Product product = new Product(getInpProductName,getInpQuantity,getInpPrice);
            products.addProduct(product);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public FormProduct(HomeFrame app, Products products ){
        this.app = app;
        this.products = products;
        formProduct();
    }

}
