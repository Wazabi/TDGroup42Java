package com.ynov.view.form;

import com.ynov.domain.User;
import com.ynov.service.Restaurants;
import com.ynov.service.Users;
import com.ynov.view.HomeFrame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FormUser extends JPanel implements ActionListener {
    private final HomeFrame app;
    private Users users;

    private static String[] tabRole = new String[] {"Administrateur", "Gestionnaire"};

    JLabel lastName = new JLabel("Nom");
    JLabel firstName = new JLabel("Prénom");
    JLabel email = new JLabel("Email");
    JLabel password = new JLabel("Mot de passe");
    JLabel role = new JLabel("Role");

    JTextField inpLastName = new JTextField(10);
    JTextField inpFirstName = new JTextField(10);
    JTextField inpEmail = new JTextField(10);
    JTextField inpPassword = new JTextField(10);

    JComboBox<String> inpRole = new JComboBox<>();

    JButton validate = new JButton("Ajouter");
    JButton backToMenu = new JButton("Retour au menu");

    public void formUser(){

        for (int i = 0; i < tabRole.length; i++){
            inpRole.addItem(tabRole[i]);
        }

        this.add(lastName);
        this.add(inpLastName);
        this.add(firstName);
        this.add(inpFirstName);
        this.add(email);
        this.add(inpEmail);
        this.add(password);
        this.add(inpPassword);
        this.add(role);
        this.add(inpRole);
        this.add(validate);
        this.add(backToMenu);

        validate.addActionListener(this);
        backToMenu.addActionListener(e -> app.menuAdmin());
    }

    public FormUser(HomeFrame app, Users users){
        this.app = app;
        this.users = users;
        formUser();
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String getInpLastName = inpLastName.getText();
        String getInpFirstName = inpFirstName.getText();
        String getInpEmail = inpEmail.getText();
        String getInpPassword = inpPassword.getText();
        String getInpRole = (String) inpRole.getSelectedItem();

        try {
            User user = new User(getInpLastName, getInpFirstName, getInpEmail, getInpPassword, getInpRole);
            users.addUser(user);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
