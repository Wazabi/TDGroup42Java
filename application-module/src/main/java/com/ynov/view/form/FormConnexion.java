package com.ynov.view.form;

import com.ynov.view.HomeFrame;

import javax.swing.*;

public class FormConnexion extends JPanel {
    private final HomeFrame app;

    JLabel email = new JLabel("Email");
    JLabel password = new JLabel("Mot de Passe");
    JLabel ipAddress = new JLabel("Adresse IP");

    JTextField inpEmail = new JTextField(15);
    JTextField inpPassword = new JTextField(10);
    JTextField inpIpAddress = new JTextField(10);

    JButton connexion = new JButton("Connection");

    public void formConnexion(){
        this.add(email);
        this.add(inpEmail);
        this.add(password);
        this.add(inpPassword);
        this.add(ipAddress);
        this.add(inpIpAddress);
        this.add(connexion);

        connexion.addActionListener(e -> app.menuGestionnaire());
    }

    public FormConnexion(HomeFrame app){

        this.app=app;
        formConnexion();
    }
}
