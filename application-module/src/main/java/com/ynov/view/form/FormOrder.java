package com.ynov.view.form;

import com.ynov.view.HomeFrame;

import javax.swing.*;

public class FormOrder extends JPanel {

    private final HomeFrame app;

    JLabel productName = new JLabel("Nom du produit");
    JLabel quantity = new JLabel("Quantité (unitaire)");
    JLabel price = new JLabel("Prix unitaire");

    JTextField inpProductName = new JTextField(15);
    JTextField inpQuantity = new JTextField(15);
    JTextField inpPrice = new JTextField(5);

    JButton orderButton = new JButton("Passer la commande");
    JButton backMenu = new JButton("Retour au menu");

    public void formOrder(){
        this.add(productName);
        this.add(inpProductName);
        this.add(quantity);
        this.add(inpQuantity);
        this.add(price);
        this.add(inpPrice);
        this.add(orderButton);
        this.add(backMenu);

        //orderButton.addActionListener(this);
        backMenu.addActionListener(e ->app.menuGestionnaire());
    }

    public FormOrder(HomeFrame app){
        this.app = app;
        formOrder();
    }

}
