package com.ynov.view.form;

import com.ynov.domain.Restaurant;
import com.ynov.service.Restaurants;
import com.ynov.view.HomeFrame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FormRestaurant extends JPanel implements ActionListener {

    private final HomeFrame app;
    private Restaurants restaurants;

    JLabel nameRestaurant = new JLabel("Nom du restaurant");
    JLabel addressRestaurant = new JLabel("Adresse du restaurant");
    JLabel user = new JLabel("Utilisateur gérant");
    JLabel phoneNumber = new JLabel("Numéro de téléphone");

    JTextField inpName = new JTextField(10);
    JTextField inpAddressRestaurant = new JTextField(10);
    JTextField inpUser = new JTextField(10);
    JTextField inpPhoneNumber = new JTextField(10);

    JButton validate = new JButton("Ajouter restaurant");
    JButton backToMenu = new JButton("Retour au menu");

    public void formRestaurant(){
        this.add(nameRestaurant);
        this.add(inpName);
        this.add(addressRestaurant);
        this.add(inpAddressRestaurant);
        this.add(user);
        this.add(inpUser);
        this.add(phoneNumber);
        this.add(inpPhoneNumber);
        this.add(validate);
        this.add(backToMenu);

        validate.addActionListener(this);
        backToMenu.addActionListener(e -> app.menuAdmin());

    }

    public FormRestaurant(HomeFrame app, Restaurants restaurants){
        this.app = app;
        this.restaurants = restaurants;
        formRestaurant();
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String getInpName = inpName.getText();
        String getInpAddressRestaurant = inpAddressRestaurant.getText();
        String getInpUser = inpUser.getText();
        int getInpPhoneNumber = Integer.parseInt(inpPhoneNumber.getText());

        try{
            Restaurant restaurant = new Restaurant(getInpName, getInpAddressRestaurant, getInpUser, getInpPhoneNumber);
            restaurants.addRestaurant(restaurant);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
