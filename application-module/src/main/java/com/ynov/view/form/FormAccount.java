package com.ynov.view.form;

import com.ynov.view.HomeFrame;

import javax.swing.*;

public class FormAccount extends JPanel{
    private final HomeFrame app;
    private static String[] tabRole = new String[] {"Administrateur", "Gestionnaire"};

    JLabel lastName = new JLabel("Nom");
    JLabel firstName = new JLabel("Prénom");
    JLabel email = new JLabel("Email");
    JLabel password = new JLabel("Mot de passe");
    JLabel role = new JLabel("Role");

    JTextField inpLastName = new JTextField(10);
    JTextField inpFirstName = new JTextField(10);
    JTextField inpEmail = new JTextField(15);
    JTextField inpPassword = new JTextField(10);

    JComboBox<String> inpRole = new JComboBox<>();

    JButton validate = new JButton("Modifier");
    JButton backToMenu = new JButton("Retour au menu");

    public void formAccount(){
        for (int i = 0; i < tabRole.length; i++){
            inpRole.addItem(tabRole[i]);
        }

        this.add(lastName);
        this.add(inpLastName);
        this.add(firstName);
        this.add(inpFirstName);
        this.add(email);
        this.add(inpEmail);
        this.add(password);
        this.add(inpPassword);
        this.add(role);
        this.add(inpRole);
        this.add(validate);
        this.add(backToMenu);

        //validate.addActionListener(this);
        backToMenu.addActionListener(e -> app.menuAdmin());
    }


    public FormAccount(HomeFrame app){

        this.app = app;
        formAccount();
    }
}
