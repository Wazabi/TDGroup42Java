package com.ynov.view.form;

import com.ynov.domain.Product;
import com.ynov.service.Products;
import com.ynov.view.HomeFrame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RemoveProduct extends JPanel implements ActionListener {
    private final HomeFrame app;
    private Products products;

    JComboBox<String> rmvProduct = new JComboBox<>();
    JButton remove = new JButton("Supprimer");
    JButton backToMenu = new JButton("Retour au menu");

    public String[] showProduct(Products products){
        Object [][] tabProduct = products.listProducts();
        String tabName[] = new String[0];
        for(int i = 0; i<tabProduct.length;i++){
            tabName[i] = String.valueOf(tabProduct[i][0]);
        }
        return tabName;
    }

    public void removeProduct(){
        for (int i = 0; i < showProduct(products).length;i++){
            rmvProduct.addItem(showProduct(products)[i]);
        }

        this.add(rmvProduct);
        this.add(remove);
        this.add(backToMenu);

        remove.addActionListener(this);
        backToMenu.addActionListener(e -> app.menuAdmin());
    }

    public RemoveProduct(HomeFrame app, Products products){
        this.app = app;
        this.products = products;
        removeProduct();
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
