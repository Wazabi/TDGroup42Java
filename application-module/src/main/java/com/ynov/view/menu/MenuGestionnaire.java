package com.ynov.view.menu;

import com.ynov.view.HomeFrame;

import javax.swing.*;

public class MenuGestionnaire extends JPanel{
    private final HomeFrame app;

    JButton productList = new JButton("Produits en Stock");
    JButton productForm = new JButton("Créer une matière première");
    JButton orderForm = new JButton("Commander un produit");
    JButton orderHistory = new JButton("Historique des commandes");
    JButton orderModify = new JButton("Modifier une commande");
    JButton accountModify = new JButton("Modifier le compte");
    JButton rmvProduct = new JButton("Supprimer un produit");
    JButton rmvOrder = new JButton("Supprimer une commande");

    public void menuGestionnaire(){
        this.add(productList);
        this.add(productForm);
        this.add(rmvProduct);
        this.add(orderForm);
        this.add(orderHistory);
        this.add(orderModify);
        this.add(rmvOrder);
        this.add(accountModify);

        productList.addActionListener(e -> app.productList());
        productForm.addActionListener(e -> app.formProduct());
        orderForm.addActionListener(e -> app.formOrder());
        orderHistory.addActionListener(e -> app.orderList());
 //       orderModify.addActionListener(e -> app.);
        accountModify.addActionListener(e -> app.formAccount());
        //rmvOrder.addActionListener(e -> app.removeProduct);
        rmvProduct.addActionListener(e -> app.removeProduct());
    }


    public MenuGestionnaire(HomeFrame app){
        this.app = app;
        menuGestionnaire();
    }
}
