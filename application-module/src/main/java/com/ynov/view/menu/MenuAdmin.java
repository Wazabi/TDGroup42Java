package com.ynov.view.menu;

import com.ynov.view.HomeFrame;

import javax.swing.*;

public class MenuAdmin extends JPanel {
    private final HomeFrame app;

    JButton userList = new JButton("Liste des utilisateurs");
    JButton restaurantForm = new JButton("Ajouter un restaurant");
    JButton userForm = new JButton("Ajouter un utilisateur");
    JButton accountModify = new JButton("Modifier le compte");
    JButton rmvUser = new JButton("Supprimer un utilisateur");
    JButton rmvRestaurant =  new JButton("Supprimer un restaurant");

    public void menuAdmin(){
        this.add(userList);
        this.add(restaurantForm);
        this.add(rmvRestaurant);
        this.add(userForm);
        this.add(rmvUser);
        this.add(accountModify);

        userList.addActionListener(e -> app.userList());
        restaurantForm.addActionListener(e -> app.formRestaurant());
        userForm.addActionListener(e -> app.formUser());
        accountModify.addActionListener(e -> app.formAccount());
//        rmvRestaurant.addActionListener(e -> );
//        rmvUser.addActionListener(e-> );
    }

    public MenuAdmin(HomeFrame app){
        this.app = app;
        menuAdmin();
    }
}
