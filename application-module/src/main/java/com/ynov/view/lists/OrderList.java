package com.ynov.view.lists;

import com.ynov.view.HomeFrame;

import javax.swing.*;

public class OrderList extends JPanel {
    private final HomeFrame app;
    String title[] = {"Produit", "Prix", "Quantité"};
    String id[][] = {};
    JTable orders;

    JButton backToMenu = new JButton("Retour au menu");

    public void orderList(){
        this.add(new JScrollPane(orders));
        this.add(backToMenu);
        backToMenu.addActionListener(e -> app.menuGestionnaire());
    }

    public OrderList(HomeFrame app){
        this.app=app;

        orders = new JTable(id,title);
        orderList();
    }
}
