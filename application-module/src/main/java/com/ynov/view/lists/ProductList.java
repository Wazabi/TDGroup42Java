package com.ynov.view.lists;

import com.ynov.domain.Product;
import com.ynov.service.Products;
import com.ynov.view.HomeFrame;

import javax.swing.*;

public class ProductList extends JPanel {
    private final HomeFrame app;
    private Products products;
    String title[] = {"Produit", "Prix", "Quantité"};
    JTable productsTable;
    JButton backToMenu = new JButton("Retour au menu");

    public void productList(){
        this.add(new JScrollPane(productsTable));
        this.add(backToMenu);
        backToMenu.addActionListener(e -> app.menuGestionnaire());
    }

    public ProductList(HomeFrame app, Products products){
        this.app = app;
        this.products = products;
        productsTable = new JTable(products.listProducts(),title);
        productList();
    }
}
