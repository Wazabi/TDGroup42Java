package com.ynov.view.lists;

import com.ynov.service.Users;
import com.ynov.view.HomeFrame;

import javax.swing.*;

public class UserList extends JPanel {
    private final HomeFrame app;
    private Users users;
    String title[] = {"Nom", "Prénom", "Email", "Role"};
    JTable usersTable;

    JButton backToMenu = new JButton("Retour au menu");

    public void userList(){
        this.add(new JScrollPane(usersTable));
        this.add(backToMenu);
        backToMenu.addActionListener(e -> app.menuAdmin());
    }

    public UserList(HomeFrame app, Users users){
        this.app=app;
        usersTable = new JTable(users.listUsers(),title);
        userList();
    }
}
