package com.ynov.view.lists;

import com.ynov.service.Restaurants;
import com.ynov.view.HomeFrame;

import javax.swing.*;

public class RestaurantList extends JPanel {
    private final HomeFrame app;
    private Restaurants restaurants;
    String title[] = {"Nom du restaurant", "Adresse", "Gérant", "n° tel"};
    JTable restaurantsTable;
    JButton backToMenu = new JButton("Retour au menu");

    public void restaurantList(){
        this.add(new JScrollPane(restaurantsTable));
        this.add(backToMenu);
        backToMenu.addActionListener(e -> app.menuGestionnaire());
    }

    public RestaurantList(HomeFrame app, Restaurants restaurants){
        this.app = app;
        this.restaurants = restaurants;
        restaurantsTable = new JTable(restaurants.listRestaurant(),title);
        restaurantList();
    }
}
