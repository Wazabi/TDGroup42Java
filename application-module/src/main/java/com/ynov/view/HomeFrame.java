package com.ynov.view;

import com.ynov.service.Products;
import com.ynov.service.Restaurants;
import com.ynov.service.Users;
import com.ynov.view.form.*;
import com.ynov.view.lists.OrderList;
import com.ynov.view.lists.ProductList;
import com.ynov.view.lists.UserList;
import com.ynov.view.menu.MenuAdmin;
import com.ynov.view.menu.MenuGestionnaire;

import javax.swing.*;
import java.awt.*;

public class HomeFrame extends JFrame {

    public static Products products = new Products();
    public static Restaurants restaurants = new Restaurants();
    public static Users users = new Users();
    public HomeFrame(Products products, Restaurants restaurants, Users users ) {
        super("Restaurant");
        this.setSize(1280, 720);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.getContentPane().setLayout(new FlowLayout());
        this.setVisible(true);
        this.products = products;
        this.restaurants = restaurants;
        this.users = users;
        connexion();
    }

    public void connexion() {
        this.getContentPane().removeAll();
        this.setContentPane(new FormConnexion(this));
        this.revalidate();
        this.repaint();
    }

    public void formRestaurant() {
        this.getContentPane().removeAll();
        this.setContentPane(new FormRestaurant(this, restaurants));
        this.revalidate();
        this.repaint();
    }

    public void formUser() {
        this.getContentPane().removeAll();
        this.setContentPane(new FormUser(this, users));
        this.revalidate();
        this.repaint();
    }

    public void formProduct() {
        this.getContentPane().removeAll();
        this.setContentPane(new FormProduct(this, products));
        this.revalidate();
        this.repaint();
    }

    public void formOrder() {
        this.getContentPane().removeAll();
        this.setContentPane(new FormOrder(this));
        this.revalidate();
        this.repaint();
    }

    public void menuAdmin() {
        this.getContentPane().removeAll();
        this.setContentPane(new MenuAdmin(this));
        this.revalidate();
        this.repaint();
    }

    public void menuGestionnaire() {
        this.getContentPane().removeAll();
        this.setContentPane(new MenuGestionnaire(this));
        this.revalidate();
        this.repaint();
    }

    public void userList() {
        this.getContentPane().removeAll();
        this.setContentPane(new UserList(this, users));
        this.revalidate();
        this.repaint();
    }

    public void orderList() {
        this.getContentPane().removeAll();
        this.setContentPane(new OrderList(this));
        this.revalidate();
        this.repaint();
    }

    public void formAccount() {
        this.getContentPane().removeAll();
        this.setContentPane(new FormAccount(this));
        this.revalidate();
        this.repaint();
    }

    public void productList() {
        this.getContentPane().removeAll();
        this.setContentPane(new ProductList(this, products));
        this.revalidate();
        this.repaint();
    }

    public void removeProduct(){
        this.getContentPane().removeAll();
        this.setContentPane(new RemoveProduct(this, products));
        this.revalidate();
        this.repaint();
    }
}