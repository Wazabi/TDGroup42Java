package com.ynov;

import com.ynov.view.HomeFrame;

import static com.ynov.view.HomeFrame.products;
import static com.ynov.view.HomeFrame.restaurants;
import static com.ynov.view.HomeFrame.users;

public class Application {
    public static void main(String[] args) {
        HomeFrame application = new HomeFrame(products, restaurants, users);
    }
}
