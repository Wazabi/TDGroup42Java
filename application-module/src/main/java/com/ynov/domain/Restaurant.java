package com.ynov.domain;

public class Restaurant {

    private static String NAME_PATTERN = "%s %s %s";

    private String nameRestaurant;
    private String addressRestaurant;
    private String user;
    private int phoneNumber;

    public Restaurant(String getInpName, String getInpAddressRestaurant, String getInpUser, int getInpPhoneNumber){
        this.nameRestaurant = nameRestaurant;
        this.addressRestaurant = addressRestaurant;
        this.user = user;
        this.phoneNumber = phoneNumber;
    }

    public String getRestaurant() {
        return String.format(NAME_PATTERN, nameRestaurant, addressRestaurant, user, phoneNumber);
    }

    public String getNameRestaurant() {
        return nameRestaurant;
    }

    public String getUser() {
        return user;
    }

    public String getAddressRestaurant() {
        return addressRestaurant;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }
}
