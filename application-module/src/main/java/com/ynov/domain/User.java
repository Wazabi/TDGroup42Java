package com.ynov.domain;

public class User {

    private static String NAME_PATTERN = "%s %s %s %s %s";

    private String lastname;
    private String firstname;
    private String email;
    private String password;
    private String role;

    public User(String lastname, String firstname, String email, String password, String role){
        this.lastname = lastname;
        this.firstname = firstname;
        this.email = email;
        this.password = password;
        this.role = role;
    }
    public String getUser() {
        return String.format(NAME_PATTERN, firstname, lastname, email, password, role);
    }

    public String getLastname() {
        return lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getEmail() { return email; }

    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }
}
