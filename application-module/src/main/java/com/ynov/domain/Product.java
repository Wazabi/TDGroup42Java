package com.ynov.domain;

public class Product {
    private static String NAME_PATTERN = "%s %s %s";

    private String productName;
    private int quantity;
    private int price;

    public Product(String productName, int quantity, int price){
        this.productName = productName;
        this.quantity = quantity;
        this.price = price;
    }
    public String getProduct(){ return String.format(NAME_PATTERN, productName, quantity, price);}

    public String getProductName() { return productName; }

    public int getQuantity(){return quantity;}

    public int getPrice() { return price; }
}
