package com.ynov.service;

import com.ynov.domain.User;

import java.util.ArrayList;
import java.util.List;

public class Users {
    private static List<User> users;

    public Users(){
        this.users = new ArrayList<>();
    }

    public void addUser(User user){
        this.users.add(user);
    }

    public static Object[][] listUsers(){
        Object[][] tableau = new Object[20][4];
        int pos = 0;
        for (User user: users){
            tableau[pos][0] = user.getLastname();
            tableau[pos][1] = user.getFirstname();
            tableau[pos][2] = user.getEmail();
            tableau[pos][3] = user.getRole();
            pos ++;
        }
        return tableau;
    }
}
