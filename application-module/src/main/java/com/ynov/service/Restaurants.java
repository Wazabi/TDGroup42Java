package com.ynov.service;

import com.ynov.domain.Restaurant;

import java.util.ArrayList;
import java.util.List;

public class Restaurants {
    private static List<Restaurant> restaurants;

    public Restaurants(){
        this.restaurants = new ArrayList<>();
    }

    public void addRestaurant(Restaurant restaurant){
        this.restaurants.add(restaurant);
    }

    public static Object[][] listRestaurant(){
        Object[][] tableau = new Object[20][4];
        int pos = 0;
        for (Restaurant restaurant: restaurants){
            tableau[pos][0] = restaurant.getNameRestaurant();
            tableau[pos][1] = restaurant.getUser();
            tableau[pos][2] = restaurant.getAddressRestaurant();
            tableau[pos][3] = restaurant.getPhoneNumber();
            pos ++;
        }
        return tableau;
    }
}
