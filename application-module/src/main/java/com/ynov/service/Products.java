package com.ynov.service;

import com.ynov.domain.Product;

import java.util.ArrayList;
import java.util.List;

public class Products {
    private static List<Product> products;

    public Products(){
        this.products = new ArrayList<>();
        listProducts();
    }

    public void addProduct(Product product){
        this.products.add(product);
    }

    public static Object[][] listProducts(){
        int pos = 0;
        Object[][] tableau = new Object[20][3];
        for (Product product: products){
            tableau[pos][0] = product.getProductName();
            tableau[pos][1] = product.getQuantity();
            tableau[pos][2] = product.getPrice();
            pos ++;
        }
        return tableau;
    }
}
