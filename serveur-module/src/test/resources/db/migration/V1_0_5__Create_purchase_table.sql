CREATE TABLE 't_purchase' {
	ID int not null,
	CUSTOMER_ID int,
	PRODUCT_NAME varchar(20),
	QUANTITY int(5),
	PRICE float()
};