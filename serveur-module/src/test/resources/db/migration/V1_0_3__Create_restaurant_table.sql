CREATE TABLE 't_restaurant' {
	ID int not null,
	NAME varchar(20),
	ADDRESS varchar(100),
	OWNER_ID int(2),
	PHONE int(10)
};