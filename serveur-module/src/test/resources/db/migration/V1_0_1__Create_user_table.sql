CREATE TABLE 't_user' {
	ID int not null,
	LAST_NAME varchar(30),
	FIRST_NAME varchar(30),
	EMAIL varchar(30),
	PASSWORD varchar(64),
	ROLE tinyint(2)
};