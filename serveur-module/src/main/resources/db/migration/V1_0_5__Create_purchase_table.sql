CREATE TABLE t_purchase (
	id INT(4) not null,
	customer_id INT(2),
	product_name VARCHAR(20),
	quantity INT(5),
	price FLOAT)
	ENGINE=InnoDB DEFAULT CHARSET=utf8;