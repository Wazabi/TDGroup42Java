CREATE TABLE t_user(
	id INT(2) not null AUTO_INCREMENT,
	last_name VARCHAR(30) NOT NULL,
	first_name VARCHAR(30) NOT NULL,
	email VARCHAR(30) NOT NULL,
	password VARCHAR(64) NOT NULL,
	role ENUM('admin', 'gestion'),
	PRIMARY KEY (id))
	ENGINE=InnoDB DEFAULT CHARSET=utf8;
