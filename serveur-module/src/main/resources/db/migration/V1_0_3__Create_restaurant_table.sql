CREATE TABLE t_restaurant (
	id INT(4) NOT NULL,
	name VARCHAR(20),
	address VARCHAR(100),
	owner_id INT(2) NOT NULL,
	phone INT(10))
	ENGINE=InnoDB DEFAULT CHARSET=utf8;