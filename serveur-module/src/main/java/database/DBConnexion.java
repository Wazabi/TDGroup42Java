package database;

import org.jdbi.v3.core.Jdbi;

public class DBConnexion
{
    private Jdbi connexionDB;
    private static String user;
    private static String password;
    private static String ipServer;

    public DBConnexion(String ipServer, String user, String password)
    {
        this.user = user;
        this.password = password;
        this.ipServer = "jdbc:mysql://" + ipServer + ":3333" + "/TdFinal";
        System.out.println("USER " + this.user + "PASSWORD " + this.password + "IP " + this.ipServer);
        Jdbi.create(this.ipServer, this.user, this.password);
        System.out.println("SUCCESS !");
    }

    private  static class DbConnexionHolder {
        private final static DBConnexion instanceDB = new DBConnexion(ipServer, user, password);
    }

    //Singleton
    public static DBConnexion getInstance()
    {
        return DbConnexionHolder.instanceDB;
    }


    public  Jdbi getConnexion()
    {
        return connexionDB;
    }
}
