package database;
import org.flywaydb.core.Flyway;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static com.sun.corba.se.impl.util.Utility.printStackTrace;

public class FlywayMigration  {
    private static String url;
    private static String username;
    private static String password;
    private static Properties prop = new Properties();
    private static InputStream inputStream = null;

    public static void main (String[] args){
        Flyway flyway = new Flyway();
        flyway.setDataSource(getUrl(), getUsername(), getPassword());
        flyway.migrate();
    }

    private static void loadInput(){
        try {
            inputStream = new FileInputStream("src/test/resources/db/config/config.properties"); /*getClass().getResourceAsStream("db/config/config.properties")*/;
            prop.load(inputStream);
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public static String getUrl(){
        loadInput();
        url = prop.getProperty("ipServer");
        System.out.println(url);
        return url;
    }

    public static String getUsername() {
        loadInput();
        username = prop.getProperty("username");
        System.out.println(username);
        return username;
    }

    public static String getPassword() {
        loadInput();
        password = prop.getProperty("password");
        System.out.println(password);
        return password;
    }

}
